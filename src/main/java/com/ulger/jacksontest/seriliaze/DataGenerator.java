package com.ulger.jacksontest.seriliaze;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public class DataGenerator {

	private List<Role> roles = new ArrayList<>();
	private Random numberPicker = new Random();
	
	public DataGenerator() {
		loadRoles();
	}
	
	public List<User> generateUsers(int count) {
		List<User> users = new ArrayList<>();
		User user;
		
		for (int i = 1; i < count+1; i++) {
			user = pickRandomUser();
			user.setId(i);
			users.add(user);
		}
		
		return users;
	}
	
	private void loadRoles() {
		roles.add(new Role(1, "Admin", "Standart admin"));
		roles.add(new Role(2, "Standart", "Normal user, only read permissions"));
		roles.add(new Role(3, "System Admin", "System admin, has all permisions"));
		roles.add(new Role(4, "Developer", "Has permissions read and write only development projects"));
	}
	
	private User pickRandomUser() {
		User user = new User();
		
		user.setName(pickUsername());
		user.setSurname(pickUsersurname());
		user.setAge(pickUserAge());
		user.setCreated(new Date());
		user.setRole(pickRandomRole());
		
		return user;
	}
	
	private String pickUsername() {
		return RandomStringUtils.randomAlphanumeric(6, 10);
//		return UserDataRepository.getNames().get(Math.abs(numberPicker.nextInt(UserDataRepository.getNames().size())));
	}
	
	private String pickUsersurname() {
		return RandomStringUtils.randomAlphanumeric(6, 10);
//		return UserDataRepository.getSurnames().get(Math.abs(numberPicker.nextInt(UserDataRepository.getSurnames().size())));
	}
	
	private int pickUserAge() {
		return numberPicker.nextInt(100);
	}
	
	private Role pickRandomRole() {
		return roles.get(numberPicker.nextInt(roles.size()));
	}
}