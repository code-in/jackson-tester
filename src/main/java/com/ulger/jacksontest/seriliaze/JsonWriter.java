package com.ulger.jacksontest.seriliaze;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonWriter {

	private static final String JSON_FILE_SUFFIX = "_source_data.json";
	private ObjectMapper objectMapper;
	
	public JsonWriter() {
		objectMapper = new ObjectMapper();
	}
	
	public void writeAsJson(Result result, String filePath) throws IOException {
		File file = null;
		FileWriter fileWriter = null;
		
		try {
			file = buildFile(getUniqueFileName(), filePath);
			fileWriter = buildFileWriter(file);
			
			writeUsersToFile(result, fileWriter);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fileWriter.close();
		}
	}
	
	private void writeUsersToFile(Result result, FileWriter fileWriter) throws IOException {
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
		String resultAsJson = objectMapper.writeValueAsString(result);
		
		fileWriter.write(resultAsJson);
	}
	
	private File buildFile(String name, String path) {
		return new File(path + File.separator + name + JSON_FILE_SUFFIX);
	}
	
	private FileWriter buildFileWriter(File file) throws IOException {
		return new FileWriter(file, true);
	}
	
	private String getUniqueFileName() {
		return Long.toString(System.currentTimeMillis());
	}
}