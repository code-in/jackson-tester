package com.ulger.jacksontest.seriliaze;

import java.util.List;

public class Result {

	private List<User> users;
	
	public Result(List<User> users) {
		this.users = users;
	}
	
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public List<User> getUsers() {
		return users;
	}
}