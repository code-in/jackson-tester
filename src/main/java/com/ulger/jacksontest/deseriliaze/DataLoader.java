package com.ulger.jacksontest.deseriliaze;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

public class DataLoader {

	public static String loadData(String fileName) throws IOException {
		InputStream inputStream = DataLoader.class.getClassLoader().getResourceAsStream(fileName);
		return IOUtils.toString(inputStream, Charset.forName("UTF-8"));
	}
}