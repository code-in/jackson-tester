package com.ulger.jacksontest.deseriliaze.pure;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PureDeseriliazer {

	private ObjectMapper objectMapper;
	
	public PureDeseriliazer() {
		objectMapper = new ObjectMapper();
	}
	
	public List<PureUser> deriliaze(String sourceJson) throws JsonParseException, JsonMappingException, IOException {
		return objectMapper.readValue(sourceJson, PureResult.class).getUsers();
	}
}