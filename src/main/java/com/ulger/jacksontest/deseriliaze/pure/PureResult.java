package com.ulger.jacksontest.deseriliaze.pure;

import java.util.List;

public class PureResult {

	private List<PureUser> users;

	public void setUsers(List<PureUser> users) {
		this.users = users;
	}
	
	public List<PureUser> getUsers() {
		return users;
	}
}