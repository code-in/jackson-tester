package com.ulger.jacksontest.deseriliaze.pure;

import java.util.Date;

public class PureUser {

	private int id;
	private String name;
	private String surname;
	private int age;
	private Date created;
	private PureRole role;
	
	public PureUser() {

	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public PureRole getRole() {
		return role;
	}

	public void setRole(PureRole role) {
		this.role = role;
	}
	
	@Override
	public String toString() {
		return 
				"id:"+ id + 
				" name:" + name + 
				"surname:" + surname + 
				"age:" + age + 
				"created:" +created+ 
				"role: {" + 
						role.toString() +
					"}";
	}
}