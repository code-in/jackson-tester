package com.ulger.jacksontest.deseriliaze.custom;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = ResultDeseriliazer.class)
public class CustomResult {

	private List<CustomUser> users;

	public CustomResult() {
		users = new ArrayList<>();
	}
	
	public void setUsers(List<CustomUser> users) {
		this.users = users;
	}
	
	public List<CustomUser> getUsers() {
		return users;
	}
	
	public void addUser(CustomUser user) {
		this.users.add(user);
	}
}