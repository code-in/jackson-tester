package com.ulger.jacksontest.deseriliaze.custom;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class RoleDeseriliazer extends JsonDeserializer<CustomRole> {

	@Override
	public CustomRole deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		return new CustomRole();
	}
}