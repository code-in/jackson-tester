package com.ulger.jacksontest.deseriliaze.custom;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class ResultDeseriliazer extends JsonDeserializer<CustomResult> {
	
	private static final String USERS = "users";
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String SURNAME = "surname";
	private static final String AGE = "age";
	private static final String CREATED = "created";
	private static final String ROLE = "role";
	private static final String DESCIRIPTION = "description";
	
	@Override
	public CustomResult deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
		JsonNode jsonNode = parser.getCodec().readTree(parser);
		JsonNode usersNode = jsonNode.get(USERS);
		
		CustomResult customResult = new CustomResult();
		
		Iterator<JsonNode> userIterator = usersNode.elements();
		
		JsonNode userNode;
		JsonNode roleNode;
		
		CustomUser customUser;
		CustomRole customRole;
		
		while(userIterator.hasNext()) {
			userNode = userIterator.next();
			roleNode = userNode.get(ROLE);
			
			customUser = new CustomUser();
			customUser.setId(userNode.get(ID).asInt());
			customUser.setName(userNode.get(NAME).asText());
			customUser.setSurname(userNode.get(SURNAME).asText());
			customUser.setAge(userNode.get(AGE).asInt());
			customUser.setCreated(new Date(userNode.get(CREATED).asLong()));
			
			customRole = new CustomRole();
			customRole.setId(roleNode.get(ID).asInt());
			customRole.setName(roleNode.get(NAME).asText());
			customRole.setDescription(roleNode.get(DESCIRIPTION).asText());
			
			customUser.setRole(customRole);
			
			customResult.addUser(customUser);
		}
		
		return customResult;
	}
}