package com.ulger.jacksontest.deseriliaze.custom;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class CustomDeseriliazer {

	private ObjectMapper objectMapper;
	
	public CustomDeseriliazer() {
		objectMapper = new ObjectMapper();
		SimpleModule module = new SimpleModule()
			.addDeserializer(CustomRole.class, new RoleDeseriliazer())
			.addDeserializer(CustomUser.class, new UserDeseriliazer())
			.addDeserializer(CustomResult.class, new ResultDeseriliazer());
		
		objectMapper.registerModule(module);
	}
	
	public List<CustomUser> deriliaze(String sourceJson) throws JsonParseException, JsonMappingException, IOException {
		return objectMapper.readValue(sourceJson, CustomResult.class).getUsers();
	}
}