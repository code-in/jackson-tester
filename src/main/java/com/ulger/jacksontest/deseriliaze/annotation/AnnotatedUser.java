package com.ulger.jacksontest.deseriliaze.annotation;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonSetter;

public class AnnotatedUser {

	private int id;
	private String name;
	private String surname;
	private int age;

	private Date created;
	private AnnotatedRole role;
	
	public int getId() {
		return id;
	}

	@JsonSetter("id")
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@JsonSetter("name")
	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	@JsonSetter("surname")
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	@JsonSetter("age")
	public void setAge(int age) {
		this.age = age;
	}

	public Date getCreated() {
		return created;
	}

	@JsonSetter("created")
	public void setCreated(Date created) {
		this.created = created;
	}

	public AnnotatedRole getRole() {
		return role;
	}

	@JsonSetter("role")
	public void setRole(AnnotatedRole role) {
		this.role = role;
	}
	
	@Override
	public String toString() {
		return 
				"id:"+ id + 
				" name:" + name + 
				"surname:" + surname + 
				"age:" + age + 
				"created:" +created+ 
				"role: {" + 
						role.toString() +
					"}";
	}
}