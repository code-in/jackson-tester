package com.ulger.jacksontest.deseriliaze.annotation;

import com.fasterxml.jackson.annotation.JsonSetter;

public class AnnotatedRole {

	private int id;
	private String name;
	private String description;

	public AnnotatedRole() {

	}
	
	public AnnotatedRole(int id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	@JsonSetter("id")
	public void setId(int id) {
		this.id = id;
	}

	@JsonSetter("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonSetter("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return 
				"id:" + getId() +
				"name:" + getName() +
				"description:" + getDescription();
	}
}