package com.ulger.jacksontest.deseriliaze.annotation;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonSetter;

public class AnnotatedResult {

	private List<AnnotatedUser> users;

	@JsonSetter("users")
	public void setUsers(List<AnnotatedUser> users) {
		this.users = users;
	}
	
	public List<AnnotatedUser> getUsers() {
		return users;
	}
}