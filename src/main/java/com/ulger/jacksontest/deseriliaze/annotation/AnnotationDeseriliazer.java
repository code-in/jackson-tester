package com.ulger.jacksontest.deseriliaze.annotation;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AnnotationDeseriliazer {

	private ObjectMapper objectMapper;
	
	public AnnotationDeseriliazer() {
		this.objectMapper = new ObjectMapper();
	}
	
	public List<AnnotatedUser> deriliaze(String sourceJson) throws JsonParseException, JsonMappingException, IOException {
		return objectMapper.readValue(sourceJson, AnnotatedResult.class).getUsers();
	}
}