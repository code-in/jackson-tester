package com.ulger.jacksontest;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ulger.jacksontest.deseriliaze.annotation.AnnotatedUser;
import com.ulger.jacksontest.deseriliaze.annotation.AnnotationDeseriliazer;
import com.ulger.jacksontest.deseriliaze.custom.CustomDeseriliazer;
import com.ulger.jacksontest.deseriliaze.custom.CustomUser;
import com.ulger.jacksontest.deseriliaze.pure.PureDeseriliazer;
import com.ulger.jacksontest.deseriliaze.pure.PureUser;
import com.ulger.jacksontest.seriliaze.DataGenerator;
import com.ulger.jacksontest.seriliaze.JsonWriter;
import com.ulger.jacksontest.seriliaze.Result;
import com.ulger.jacksontest.seriliaze.User;

public class Application {

	public static void main(String[] args) throws IOException {
//		testPureDeseriliazing(args, DataLoader.loadData("1541070043883_source_data.json"));
//		testAnnotationDeseriliazing(args, DataLoader.loadData("1541070043883_source_data.json"));
//		testCustomDeseriliazing(args, DataLoader.loadData("1541070043883_source_data.json"));
//		generateData(args);
	}
	
	private static void generateData(String[] args) throws IOException {
		int userCount = 200000;
		
		DataGenerator dataGenerator = new DataGenerator();
		List<User> users = dataGenerator.generateUsers(userCount);
		
		JsonWriter jsonWriter = new JsonWriter();
		jsonWriter.writeAsJson(new Result(users), "D:\\Users\\ahmet.ulger\\Desktop\\json_output");
	}
	
	private static void testPureDeseriliazing(String[] args, String source) throws JsonParseException, JsonMappingException, IOException {
		long start = System.currentTimeMillis();
		List<PureUser> users = new PureDeseriliazer().deriliaze(source);
		for (PureUser user : users)
			user.toString();
		long end = System.currentTimeMillis();
		
		System.out.println("\nPure deseriliazing completed in milliseconds: " + (end - start) );
		System.out.println("\nTotal row count: " + users.size());
	}
	
	private static void testAnnotationDeseriliazing(String[] args, String source)  throws JsonParseException, JsonMappingException, IOException {
		long start = System.currentTimeMillis();
		List<AnnotatedUser> users = new AnnotationDeseriliazer().deriliaze(source);
		for (AnnotatedUser user : users)
			user.toString();
		long end = System.currentTimeMillis();
		
		System.out.println("\nAnnotation deseriliazing completed in milliseconds: " + (end - start) );
		System.out.println("\nTotal row count: " + users.size());
	}
	
	private static void testCustomDeseriliazing(String[] args, String source)  throws JsonParseException, JsonMappingException, IOException {
		long start = System.currentTimeMillis();
		List<CustomUser> users = new CustomDeseriliazer().deriliaze(source);
		for (CustomUser user : users)
			user.toString();
		long end = System.currentTimeMillis();
		
		System.out.println("\nCustom deseriliazing completed in milliseconds: " + (end - start) );
		System.out.println("\nTotal row count: " + users.size());
	}
}